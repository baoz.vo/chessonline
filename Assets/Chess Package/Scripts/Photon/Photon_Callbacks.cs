﻿using Photon.Pun;
using Photon.Realtime;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Photon_Callbacks : Photon_Helper
{
    private Photon_RPC photon_RPC;

    public override bool Check_Network()
    {
        if (PhotonNetwork.IsConnected)
            return true;
        else return false;
    }

    public override bool Check_Lobby_Joined()
    {
        if (PhotonNetwork.InLobby)
            return true;
        else return false;
    }

    public override bool Check_On_Close_Room()
    {
        if (PhotonNetwork.PlayerList.Length == PhotonNetwork.CurrentRoom.MaxPlayers)
        {
            return true;
        }
        else return false;
    }

    public override void Connect_To_Mater()
    {
        Debug.Log("I connecting to photon server...");
        PhotonNetwork.NickName = "Player " + Random.Range(0, 10000);
        PhotonNetwork.ConnectUsingSettings();//Kết nối tới server
    }

    public override void Join_Lobby()
    {
        Debug.Log("I am joining lobby");
        PhotonNetwork.JoinLobby();//Gọi hàm photon join lobby
    }

    public override void On_Joied_Lobby()
    {
        Photon_State.On_Join_Lobby();//Cập nhật tình trạng join lobby
    }

    public override void On_Connected_State()
    {
        Debug.Log("I connected to photon server");
        Photon_State.On_Connected();//Cập nhật tình trạng connect
    }

    public override void On_Diconnected_State()
    {
        Debug.Log("I disconnected to photon server");
        Photon_State.On_Diconnected();//Cập nhật tình trạng connect
    }

    public override void Create_Random_Room()
    {
        int randomRoomName = Random.Range(0, 10000);

        //Tạo kiểu lobby để dùng sql filter
        TypedLobby sqlLobby = new TypedLobby(Photon_Property.LobbyProperty.autoMatchLobbyType, LobbyType.SqlLobby);//Chỉ có auto match mới dùng lobbytype để filter room
        RoomOptions roomOptions = new RoomOptions() { IsVisible = true, IsOpen = true, MaxPlayers = (byte)Photon_Var.max_player_in_room, EmptyRoomTtl = 0 };//Thiết lập thông tin phòng

        //Tạo hash table lưu trữ data của 1 room, dùng cả trong match
        roomOptions.CustomRoomProperties = new ExitGames.Client.Photon.Hashtable();
        roomOptions.CustomRoomProperties.Add(Photon_Property.RoomProperty.BlueScore, 0);
        roomOptions.CustomRoomProperties.Add(Photon_Property.RoomProperty.RedScore, 0);
        roomOptions.CustomRoomProperties.Add(Photon_Property.RoomProperty.Mode, 0);

        //Khai báo room option cho lobby thấy
        roomOptions.CustomRoomPropertiesForLobby = new string[1]
        {
                Photon_Property.RoomProperty.Mode
        };

        Debug.Log("Tried to create a new room");
        PhotonNetwork.CreateRoom("Room" + randomRoomName, roomOptions, sqlLobby);//Tạo room theo điểu kiện trên
        Photon_State.On_Enter_Random_Match();
        Photon_UI_Delegate.instance.Disable_Random_Match_Button();
    }

    public override void Join_Random_Room()
    {
        if (Photon_State.Get_Connected_Master_State() && Photon_State.Get_Joined_Lobby_State())
        {
            Debug.Log("I am on joining random match");
            TypedLobby sqlLobby = new TypedLobby(Photon_Property.LobbyProperty.autoMatchLobbyType, LobbyType.SqlLobby);//Khai báo kiểu lobby để filter
            MatchmakingMode matchmakingMode = MatchmakingMode.RandomMatching;//Kiểu auto match
            string sqlLobbyFilter = Photon_Property.RoomProperty.Mode + " = 0";//Lọc theo điểu kiện (chỉ có auto match)
            PhotonNetwork.JoinRandomRoom(null, (byte)Photon_Var.max_player_in_room, matchmakingMode, sqlLobby, sqlLobbyFilter);//Join ngẫu nhiên theo điều kiện nêu trên
        }
    }

    public override void Join_Specific_Room(string roomName)
    {
        PhotonNetwork.JoinRoom(roomName);
    }

    public override void Create_Custom_Room()
    {
        if (Photon_State.Get_Connected_Master_State() && Photon_State.Get_Joined_Lobby_State())//Nếu đã connect đến server và joined lobby thì cho tạo room
        {
            RoomOptions roomOptions = new RoomOptions() { IsVisible = true, IsOpen = true, MaxPlayers = (byte)Photon_Var.max_player_in_room, EmptyRoomTtl = 0 };

            //Tạo hash table lưu trữ data của 1 room, dùng cả trong match
            roomOptions.CustomRoomProperties = new ExitGames.Client.Photon.Hashtable();
            roomOptions.CustomRoomProperties.Add(Photon_Property.RoomProperty.BlueScore, 0);
            roomOptions.CustomRoomProperties.Add(Photon_Property.RoomProperty.RedScore, 0);
            roomOptions.CustomRoomProperties.Add(Photon_Property.RoomProperty.Mode, 1);

            //Khai báo room option cho lobby thấy
            roomOptions.CustomRoomPropertiesForLobby = new string[1]
            {
                Photon_Property.RoomProperty.Mode
            };

            Debug.Log("I am going to create a custom room name: " + Photon_UI_Delegate.instance.roomNameInput.text.ToString());
            PhotonNetwork.CreateRoom(Photon_UI_Delegate.instance.roomNameInput.text.ToString(), roomOptions);
            Photon_State.On_Enter_Custom_Match();
            Photon_UI_Delegate.instance.Disable_Custom_Match_Button();
        }
    }

    public override void On_Another_Player_Entered_Room()
    {
        if (Check_On_Close_Room())
        {
            On_Room_Close();
        }
    }

    public override void On_Room_Close()
    {
        //Close room
        PhotonNetwork.CurrentRoom.IsOpen = false;
        PhotonNetwork.CurrentRoom.IsVisible = false;
        //Send data
        photon_RPC = new Photon_RPC(Photon_Match_Spawning.instance.PV);
        Photon_State.On_Wait_To_Start_Match();
        photon_RPC.Send_Start_Match(Photon_State.on_wait_to_start_match);
    }

    public override void On_Joined_Room(int scene_index)
    {
        Debug.Log("I am on join and load scene success");
        PhotonNetwork.LoadLevel(scene_index);
    }

    public override void On_Match_Scene_Finished_Loading(Scene scene, LoadSceneMode mode)//Hàm này chạy khi đã load scene thành công
    {
        Photon_State.Update_Scene(scene.buildIndex);//Cập nhật scene index hiện tại
        if (Photon_State.Get_Current_Scene() == Photon_Var.match_scene_index)
        {
            Debug.Log("I load match scene success");
            
            //Tạo photon player prefab
            Create_Photon_Player();
        }
    }

    public override void Create_Photon_Player()
    {
        GameObject photon_player = PhotonNetwork.Instantiate(Path.Combine("Photon Player", "PlayerAvatar"), Vector3.zero, Quaternion.identity, 0);
        photon_player.GetComponent<Photon_Player>().Set_Team();
        Photon_Match_Spawning.control.Register_Photon_Player(photon_player.GetComponent<Photon_Player>());
    }

    //private void DebugTest()
    //{
    //    foreach (Player player in PhotonNetwork.PlayerList)
    //    {
    //        Photon_Debug.instance.AddDebugLog(player.NickName);
    //    }
    //}
}
