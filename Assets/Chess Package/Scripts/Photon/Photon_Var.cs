﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Photon_Var
{
    public static LayerMask chess_mask = ~9;
    public static LayerMask square_mask = ~10;
    public static int delta_square_axis = 2;
    public static int custom_match_index = 1;
    public static int match_scene_index = 1;
    public static int max_player_in_room = 2;
    public static float time_wait_to_start_match = 1f;
}
