﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Photon_Match_Spawning : MonoBehaviour
{
    public static Photon_Match_Spawning instance;
    public static Photon_Match_Control control = new Photon_Match_Control();
    public Click_Detecter click_detecter = new Click_Detecter();
    private Photon_RPC photon_RPC;
    public PhotonView PV;
    public Photon_Player photon_player;

    public bool on_match_processing;
    public bool on_switch_king_and_rook;//Nước đi đặc biệt phức tạp nên cần biến toàn cục để quản lý tiến trình
    public bool on_catching_pawn;//Nước đi đặc biệt phức tạp nên cần biến toàn cục để quản lý tiến trình
    public bool on_pawn_revolution;//Nước đi đặc biệt phức tạp nên cần biến toàn cục để quản lý tiến trình

    private void Awake()
    {
        instance = this;
        PV = GetComponent<PhotonView>();
        photon_RPC = new Photon_RPC(PV);
    }

    void Update()
    {
        click_detecter.Step_Control();

        if (Photon_State.on_wait_to_start_match)
        {
            if (PhotonNetwork.IsMasterClient)
            {
                if (!Photon_State.on_setup_scene_object)
                {
                    Photon_State.on_setup_scene_object = true;

                    control.Spawn_Square();

                    StartCoroutine(control.Yield_Wait_To_Start_Game());
                }
            }
        }
    }

    /// <summary>
    /// Cho phép tiến trình trong game tiếp tục (liên quan đến hành động)
    /// </summary>
    public void Enable_Match_Proccess()
    {
        on_match_processing = true;
        photon_RPC.Send_Match_Processing(on_match_processing);
    }

    /// <summary>
    /// Dừng hoàn toàn tiến trình trong game (liên quan đến hành động)
    /// </summary>
    public void Stop_Match_Proccess()
    {
        on_match_processing = false;
        photon_RPC.Send_Match_Processing(on_match_processing);
    }

    /// <summary>
    /// Kích hoạt tiến trình nhập thành
    /// </summary>
    public void Enable_Switch_King_Proccess()
    {
        on_switch_king_and_rook = true;
        photon_RPC.Send_Switch_King_Processing(on_switch_king_and_rook);
    }

    /// <summary>
    /// Kết thúc tiến trình nhập thành
    /// </summary>
    public void Disable_Switch_King_Proccess()
    {
        on_switch_king_and_rook = false;
        photon_RPC.Send_Switch_King_Processing(on_switch_king_and_rook);
    }

    /// <summary>
    /// Kích hoạt tiến trình bắt tốt qua đường
    /// </summary>
    public void Enable_Catch_Pawn_Proccess()
    {
        on_catching_pawn = true;
        photon_RPC.Send_Catch_Pawn_Processing(on_catching_pawn);
    }

    /// <summary>
    /// Kết thúc tiến trình bắt tốt qua đường
    /// </summary>
    public void Disable_Catch_Pawn_Proccess()
    {
        on_catching_pawn = false;
        photon_RPC.Send_Catch_Pawn_Processing(on_catching_pawn);
    }

    /// <summary>
    /// Kích hoạt tiến trình phong hậu
    /// </summary>
    public void Enable_Pawn_Revolution_Proccess()
    {
        on_pawn_revolution = true;
        photon_RPC.Send_Pawn_Revolution_Processing(on_pawn_revolution);
    }

    /// <summary>
    /// Kết thúc tiến trình phong hậu
    /// </summary>
    public void Disable_Pawn_Revolution_Proccess()
    {
        on_pawn_revolution = false;
        photon_RPC.Send_Pawn_Revolution_Processing(on_pawn_revolution);
    }

    [PunRPC]
    void RPC_Send_Start_Match(bool value)
    {
        Photon_State.on_wait_to_start_match = value;
    }

    [PunRPC]
    void RPC_Send_Target(Vector3 target)
    {
        click_detecter.dir_pos = target;
    }

    [PunRPC]
    void RPC_Match_Proccessing(bool proccess)
    {
        on_match_processing = proccess;
    }

    [PunRPC]
    void RPC_Switch_King_Processing(bool proccess)
    {
        on_switch_king_and_rook = proccess;
    }

    [PunRPC]
    void RPC_Catch_Pawn_Processing(bool proccess)
    {
        on_catching_pawn = proccess;
    }

    [PunRPC]
    void RPC_Pawn_Revolution_Processing(bool proccess)
    {
        on_pawn_revolution = proccess;
    }
}
