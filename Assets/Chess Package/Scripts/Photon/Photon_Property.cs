﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Photon_Property
{
    public class RoomProperty
    {
        public const string RedScore = "RS";
        public const string BlueScore = "BS";
        public const string Mode = "C1";
    }

    public class LobbyProperty
    {
        public const string autoMatchLobbyType = "autoMatchLobby";
        public const string customMatchLobbyType = "customMatchLobby";
    }
}
