﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Photon_Event_Click : MonoBehaviour
{
    public static Photon_Event_Click instance;

    private void Awake()
    {
        instance = this;
    }

    /// <summary>
    /// Event button
    /// </summary>
    public void On_Join_Random_Game()//Gọi bên class Photon_UI_Delegate
    {
        Debug.Log("I am loading match scene");
        Photon_Lobby.photon_callbacks.Join_Random_Room();
    }

    public void On_Join_Custom_Game()//Gọi bên class Photon_UI_Delegate
    {
        Debug.Log("I am loading match scene");
        Photon_Lobby.photon_callbacks.Create_Custom_Room();
    }

    public void Join_Specific_Room(string roomName)//Gọi bên class Photon_Room_Listing
    {
        Debug.Log("I am joining specific room with name");
        Photon_Lobby.photon_callbacks.Join_Specific_Room(roomName);
    }
}
