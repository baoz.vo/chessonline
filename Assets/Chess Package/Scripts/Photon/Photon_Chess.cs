﻿using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Photon_Chess : MonoBehaviour
{
    public Photon_Chess_Handler chess_handler = new Photon_Chess_Handler();
    public PhotonView PV;
    public Photon_RPC photon_RPC;

    private void Awake()
    {
        PV = GetComponent<PhotonView>();
        photon_RPC = new Photon_RPC(PV);
        chess_handler.Set_Parent(transform);
    }

    private void Update()
    {
        if (PhotonNetwork.IsMasterClient)//Chỉ có master client mới có thể kiểm soát scene object (cũng như các biến và animation của chess)
        {
            chess_handler.Handle_State();
            chess_handler.Handle_Animation();
            chess_handler.Transform_To_Destination(Photon_Match_Spawning.instance.click_detecter.dir_pos);
            chess_handler.Transform_To_Attack(Photon_Match_Spawning.instance.click_detecter.dir_pos);
        }
    }

    /// <summary>
    /// Set team cho chess và sync lên server
    /// </summary>
    public void Set_Team(int myTeam)
    {
        chess_handler.myTeam = myTeam;
        photon_RPC.Send_Chess_Team(chess_handler.myTeam);
    }

    public void Revolution_On_Time()//Gọi bên animation event
    {
        chess_handler.On_Pawn_Revolution();
    }

    public void Revolution_End_Time()//Gọi bên animation event
    {
        chess_handler.Done_Pawn_Revolution();
    }

    /// <summary>
    /// Gọi khi kết thúc animation đánh
    /// </summary>
    public void Disable_Attack()//Gọi bên animation event
    {
        chess_handler.attack = false;
        chess_handler.done_attack = true;
        if (chess_handler.on_catch_pawn)
        {
            chess_handler.on_catch_pawn = false;
            Photon_Match_Spawning.instance.Disable_Catch_Pawn_Proccess();
            chess_handler.Return_To_Origin_Rotation(Quaternion.Euler(chess_handler.origin_rot));
        }
    }

    /// <summary>
    /// Gọi khi chess bắt đầu tấn công chess khác
    /// </summary>
    public void Enable_Hitting()//Gọi bên animation event
    {
        Photon_Chess chess = Photon_Match_Spawning.control.Find_Chess(Photon_Match_Spawning.instance.click_detecter.dir_pos);
        chess.chess_handler.Death_State();
        Photon_Match_Spawning.instance.Stop_Match_Proccess();//Dừng tất cả hoạt động liên quan cho tới khi từng chess thực thi xong hành động của mình
    }

    /// <summary>
    /// Gọi khi kết thúc animation death
    /// </summary>
    public void Destroying()//Gọi bên animation event
    {
        Destroy_Chess(Photon_Match_Spawning.instance.click_detecter.dir_pos);
    }

    /// <summary>
    /// Destroy hoàn toàn object trên server
    /// </summary>
    private void Destroy_Chess(Vector3 target)
    {
        Photon_Chess chess = Photon_Match_Spawning.control.Find_Chess(new Vector3(Convert.ToInt32(target.x), Convert.ToInt32(target.y), Convert.ToInt32(target.z)));
        if (chess != null)
        {
            Photon_Match_Spawning.control.Photon_Destroy_Object_Helper(chess.gameObject);
            Photon_Match_Spawning.instance.Enable_Match_Proccess();//Cho phép các chess thực thi hành động của mình
        }
    }

    /// <summary>
    /// Gọi hàm dừng chờ bất kỳ
    /// </summary>
    public void Call_Wait(IEnumerator funtion)
    {
        StartCoroutine(funtion);
    }

    [PunRPC]
    void RPC_Send_Step_Count(int count)
    {
        chess_handler.my_Step_Count = count;
    }

    [PunRPC]
    void RPC_Send_Chess_Switch_Time(int count)
    {
        chess_handler.my_Switch_Time = count;
    }

    [PunRPC]
    void RPC_Pawn_Catch(bool catched)
    {
        chess_handler.can_be_catched = catched;
    }

    [PunRPC]
    void RPC_King_Switched(bool switched)
    {
        chess_handler.on_switched = switched;
    }

    [PunRPC]
    void RPC_Send_Chess_Team(int myTeam)
    {
        chess_handler.myTeam = myTeam;
    }

    [PunRPC]
    void RPC_Send_Transform(bool transform)
    {
        chess_handler.on_transform = transform;
    }

    [PunRPC]
    void RPC_Send_Transform_To_Attack(bool transform_to_attack)
    {
        chess_handler.on_transform_to_attack = transform_to_attack;
    }
}
