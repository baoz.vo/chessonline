﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Photon_Room_Listing : MonoBehaviour
{
    public string room_name;
    public int room_size;
    public int current_players;

    public TextMeshProUGUI nameText;
    public TextMeshProUGUI sizeText;
    public TextMeshProUGUI currentNumberText;

    private void Awake()
    {
        this.GetComponent<Button>().onClick.AddListener(delegate () { On_Click_Join_Room(room_name); });
    }

    private void Update()
    {
        nameText.text = room_name;
        sizeText.text = room_size.ToString();
        currentNumberText.text = current_players.ToString();
    }

    public void Set_Room(string name, int size, int amount)
    {
        room_name = name;
        room_size = size;
        current_players = amount;
    }

    private void On_Click_Join_Room(string room_name)
    {
        this.GetComponent<Button>().enabled = false;
        Photon_Event_Click.instance.Join_Specific_Room(room_name);
    }
}
