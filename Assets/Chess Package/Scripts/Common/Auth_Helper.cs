﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public abstract class Auth_Helper: Send_Helper
{
    /// <summary>
    /// Photon_Step Var
    /// </summary>
    public string step_storage;
    protected int prev_length;
    protected int count;
    protected int team;
    protected int chess_type;
    protected GameObject step_ui = null;
    public int current_team_permission;

    /// <summary>
    /// Photon_Step
    /// </summary>
    public abstract void Refresh_Steps(Transform blue, Transform red);

    public abstract void Save_Step(PhotonView PV, int my_team, Chess_Type chess_Type, Vector3 pos);

    public abstract void UI_SetUp(GameObject step_prefab, Transform blue_team_steps_content, Transform red_team_steps_content, int team, int type, int x, int y, int z);

    public abstract void UI_Loading(GameObject step_prefab, Transform blue_team_steps_content, Transform red_team_steps_content);

    public abstract void Update_Team_Permission(PhotonView PV);
}
