﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TabManager : MonoBehaviour
{
    public List<Tab_Control> tab_Controls;

    public static TabManager manager;

    private void Awake()
    {
        manager = this;
    }
}
