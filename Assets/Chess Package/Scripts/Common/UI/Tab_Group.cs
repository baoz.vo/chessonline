﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[System.Serializable]
public class Tab_Group
{
    public Tab[] tabs;
    public List<Page> pages = new List<Page>();
    public List<Slot> slots = new List<Slot>();
    public Transform tabs_parent;
    public Transform pages_parent;

    public int page_index = 0;

    public Tab_Group(Transform _tabs_parent, Transform _pages_parent)
    {
        tabs_parent = _tabs_parent;
        pages_parent = _pages_parent;
        Collect_Tabs();
        Collect_Pages();
        Collect_Slots();
    }

    public void Collect_Slots()
    {
        foreach (var child in GameObject.FindObjectsOfType<Slot>())
        {
            slots.Add(child);
        }
    }

    public void Collect_Pages()
    {
        for (int i = 0; i < pages_parent.childCount; i++)
        {
            if(pages_parent.GetChild(i).GetComponent<Page>() != null)
            {
                pages.Add(pages_parent.GetChild(i).GetComponent<Page>());
            }
        }
    }

    public void Collect_Tabs()
    {
        tabs = tabs_parent.GetComponentsInChildren<Tab>(true);
    }

    public void Un_Choose_Other_Tabs(Tab current_tab)
    {
        foreach (Tab tab in tabs)
        {
            if(tab != current_tab)
            {
                tab.None_State();
                tab.Un_Scale_Text();
                Hide_Page(tab);
            }
        }
    }

    public void Un_Choose_Other_Slots(Slot slot)
    {
        foreach (Slot child in slots)
        {
            if(child != slot)
            {
                child.Normal_Slot();
            }
        }
    }

    public Page Find_Page(Tab tab)
    {
        int index = tabs.Select((s, i) => new { i, s }).Where(m => m.s == tab).Select(t => t.i).Single();
        return pages[index];
    }

    public void Show_Page(Tab tab)
    {
        Page page = Find_Page(tab);
        page.gameObject.SetActive(true);
    }

    public void Hide_Page(Tab tab)
    {
        Page page = Find_Page(tab);
        page.gameObject.SetActive(false);
    }
}
