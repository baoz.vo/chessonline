﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public abstract class Effect_Helper
{
    protected GameObject Spawn_Particle(Transform parent, GameObject particle_prefab, Vector3 pos, Quaternion rot)
    {
        GameObject particle = Photon_Match_Spawning.control.Photon_Instantiate_Helper(Path.Combine("Effect Prefabs", "Dead Effect", particle_prefab.name), pos, rot, 0);

        return particle;
    }

    protected void Evolution_Effect(Transform parent)
    {
        Photon_Match_Spawning.control.Photon_Instantiate_Helper(Path.Combine("Effect Prefabs", "Evolution Effect", "Evolution Effect"),
            parent.transform.position + new Vector3(0, 0.1f, 0), parent.transform.rotation, 0);
    }
}
