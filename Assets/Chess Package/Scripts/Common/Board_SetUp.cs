﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Board_SetUp : MonoBehaviour
{
    public static Board_SetUp board;

    public Board_Handler board_Handler = new Board_Handler();

    private void Awake()
    {
        board = this;
    }

    private void Start()
    {
        board_Handler.Board_SetUp();

        board_Handler.Board_Debug_Test();
    }
}
