﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game_Manager : MonoBehaviour
{
    public static Game_Manager manager;

    private void Awake()
    {
        if (Game_Manager.manager == null)
        {
            Game_Manager.manager = this;
        }
        else
        {
            if (Game_Manager.manager != this)
            {
                Destroy(Game_Manager.manager.gameObject);
                Game_Manager.manager = this;
            }
        }
        DontDestroyOnLoad(this.gameObject);
    }

    private void Start()
    {
        Input_Manager.Load_All_Key();
    }
}
