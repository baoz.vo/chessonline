﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface Helper
{
    GameObject Photon_Instantiate_Helper(string prefabName, Vector3 pos, Quaternion rot, byte group);

    GameObject Mono_Instantiate_Helper(GameObject spawn_obj, Vector3 pos, Quaternion rot);
}

public abstract class Spawn_Helper: Send_Helper, Helper
{
    protected int team_spawner = -1;

    public GameObject Photon_Instantiate_Helper(string prefabName, Vector3 pos, Quaternion rot, byte group)
    {
        return PhotonNetwork.Instantiate(prefabName, pos, rot, group);
    }

    public void Photon_Destroy_Object_Helper(GameObject chess)
    {
        PhotonNetwork.RemoveRPCs(chess.GetComponent<PhotonView>());
        PhotonNetwork.Destroy(chess);
    }

    public GameObject Mono_Instantiate_Helper(GameObject spawn_obj, Vector3 pos, Quaternion rot)
    {
        return MonoBehaviour.Instantiate(spawn_obj, pos, rot);
    }

    protected Chess_SetUp[] Get_Photon_Objects()
    {
        return GameObject.FindObjectsOfType<Chess_SetUp>();
    }

    protected void Assign_Photon_Player(Photon_Player player)
    {
        Photon_Match_Spawning.instance.photon_player = player;
    }

    public abstract IEnumerator Yield_Wait_To_Start_Game();

    public abstract void Set_Up_Scene_Object();

    public abstract Quaternion Get_SetUp_Rotation(Chess_SetUp scene_object);

    public abstract GameObject[] Get_All_Square_Object();

    public abstract Photon_Chess[] Get_All_Photon_Chess();

    public abstract Photon_Chess Find_Chess(Vector3 position);

    public abstract Square[] Get_All_Square_Script();

    public abstract Square Find_Square(Vector3 position);

    public abstract void Spawn_Square();
}
